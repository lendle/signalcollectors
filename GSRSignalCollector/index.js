var SerialPort=require("serialport").SerialPort;
var request=require('request');
var serialport=new SerialPort("/dev/ttyS0", {
	baudrate: 57600
});

serialport.on("open", function(){
	//serialport.write("love zoe\n");
	//serialport.close();
	console.log("open!");
	serialport.on('data', function(data){
		process.stdout.write(data.toString('utf-8'));
		var packet={
			time: new Date().getTime(),
			data: parseInt(data.toString('utf-8'))
		};
		request.post("http://imsofa.rocks:8080/LabUtils/ws/app-messages", {
			json:{
				applicationId: process.argv[2],
				content: JSON.stringify(packet),
				timestamp: packet.time
			}
		});
	});
});

