var NodeWebcam = require("node-webcam");
var request = require('request');

//Default options
var opts = {
    width: 640,
    height: 480,
    quality: 100,
    //Delay to take shot
    delay: 0,
    //Save shots in memory
    saveShots: true,
    // [jpeg, png] support varies
    // Webcam.OutputTypes
    output: "png",
    //Which camera to use
    //Use Webcam.list() for results
    //false for default device
    device: false,
    // [location, buffer, base64]
    // Webcam.CallbackReturnTypes
    callbackReturn: "base64",
    //Logging
    verbose: true,
    skip: 2
};

var applicationId = process.argv[2];
var interval = parseInt(process.argv[3]);
var usesample = (process.argv.length >= 5 && process.argv[4] == "true");
//Creates webcam instance
var Webcam = null;
setTimeout(capture, interval);

function calculateDistance(point1, point2){
    return Math.pow(point1[0]-point2[0], 2)+Math.pow(point1[1]-point2[1], 2);
}

function capture() {
    if (usesample) {
        request.post("http://imsofa.rocks:8080/LabUtils/ws/app-messages", {
            json: {
                "applicationId": applicationId,
                content: JSON.stringify({
                    mlel: 0.5,
                    mrer: 0.6,
                    nlml: 0.5,
                    nrmr: 0.8,
                    nlel: 0.5,
                    nrer: 0.3
                })
            }
        }, function () {
            setTimeout(capture, interval);
        });
    } else {
        if(Webcam==null){
            Webcam = NodeWebcam.create(opts);
        }
        Webcam.capture("test_picture", function (err, data) {
            var index = data.indexOf(",");
            data = data.substr(index + 1);
            request.post("http://imsofa.rocks:8080/LabUtils/face/landmarks", {
                form: {
                    "image": data,
                    "key": "imsofarocks",
                    "timestamp": "" + new Date().getTime()
                }
            }, function (err, response, body) {
                if (body && ("" + body).length > 5) {
                    try{
                        let landmarks=JSON.parse(body).value.landmarks;
                        let box=JSON.parse(body).value.largestFaceBoundingBox;
                        let area=box.width*box.height;
                        let ret={
                            mlel: calculateDistance(landmarks.MouthLeft, landmarks.EyeLeftOuter)/area,
                            mrer: calculateDistance(landmarks.MouthRight, landmarks.EyeRightOuter)/area,
                            nlml: calculateDistance(landmarks.NoseLeftAlarOutTip, landmarks.MouthLeft)/area,
                            nrmr: calculateDistance(landmarks.NoseRightAlarOutTip, landmarks.MouthRight)/area,
                            nlel: calculateDistance(landmarks.NoseLeftAlarOutTip, landmarks.EyeLeftOuter)/area,
                            nrer: calculateDistance(landmarks.NoseRightAlarOutTip, landmarks.EyeRightOuter)/area
                        };
                        request.post("http://imsofa.rocks:8080/LabUtils/ws/app-messages", {
                            json: {
                                "applicationId": applicationId,
                                content: JSON.stringify(ret),
                                timestamp: new Date().getTime()
                            }
                        }, function () {
                            setTimeout(capture, interval);
                        });
                    }catch(e){
                        setTimeout(capture, interval);
                    }
                } else {
                    setTimeout(capture, interval);
                }
            });
        });
    }
}