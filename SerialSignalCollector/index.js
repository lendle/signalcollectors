var port="/dev/ttyS0";
var baudrate=57600;

if(process.argv && process.argv.length>=4){
	port=process.argv[3];
}
if(process.argv && process.argv.length>=5){
	baudrate=parseInt(process.argv[4]);
}

console.log(process.argv);

var SerialPort=require("serialport").SerialPort;
var request=require('request');
var serialport=new SerialPort(port, {
	'baudrate': baudrate
});

serialport.on("open", function(){
	//serialport.write("love zoe\n");
	//serialport.close();
	console.log("open!");
	serialport.on('data', function(data){
		process.stdout.write(data.toString('utf-8'));
		var packet={
			time: new Date().getTime(),
			data: parseInt(data.toString('utf-8'))
		};
		request.post("http://imsofa.rocks:8080/LabUtils/ws/app-messages", {
			json:{
				applicationId: process.argv[2],
				content: JSON.stringify(packet),
				timestamp: packet.time
			}
		});
	});
});

