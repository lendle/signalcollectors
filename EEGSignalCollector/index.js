var neurosky = require('node-neurosky');
var client = neurosky.createClient({
	appName: 'test',
	appKey: 'A94A8FE5CCB19BA61C4C0873D391E987982FBBD3'
});

var request=require("request");

client.on('data',function(data){
	console.log(data);
	if("eSense" in data){
		var sendingMessage={};
		for(var key in data.eSense){
			sendingMessage[key]=data.eSense[key];
		}
		for(var key in data.eegPower){
			sendingMessage[key]=data.eegPower[key];
		}
		sendingMessage["poorSignalLevel"]=data.poorSignalLevel;
		sendingMessage['timeStamp']=new Date().getTime();
		
		request.post("http://imsofa.rocks:8080/LabUtils/ws/app-messages", {
			json: {
				"applicationId":process.argv[2],
				"content": JSON.stringify(sendingMessage),
				timestamp: sendingMessage['timeStamp']
			}
		});
		//console.log("message sent: "+JSON.stringify(sendingMessage));
	}
});

client.connect();